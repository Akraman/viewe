package com.project.viewe.dto;

import lombok.Data;

@Data
public class CommentDto {
    private String text;
}
