package com.project.viewe.dto;

import lombok.Data;

@Data
public class VisualCommentDto {
    private String username;
    private String photoUrl;
    private String videoUrl;
    private String text;
}
